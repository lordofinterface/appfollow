import fakeData from "../helpers/fakeData";

export default {
  getData(url) {
    // imitate http request
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(fakeData[url])
      }, 0);
    });
  }
};
