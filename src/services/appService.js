import httpService from '../services/httpService';

const API = {
  BANNERS: 'top_banners',
  APPS: 'apps',
  BANNER_COLLECTIONS: 'banner_collections',
  APP_COLLECTIONS: 'app_collections',
  TOP_APPS: 'top_apps',
}

export default {
  getBanners(device) {
    return httpService.getData(`${API.BANNERS}?device=${device}`);
  },
  getApps(device) {
    return httpService.getData(`${API.APPS}?device=${device}`);
  },
  getBannerCollections() {
    return httpService.getData(`${API.BANNER_COLLECTIONS}`);
  },
  getAppCollections(device) {
    return httpService.getData(`${API.APP_COLLECTIONS}?device=${device}`);
  },
  getTopFree(device) {
    return httpService.getData(`${API.TOP_APPS}?device=${device}&type=free`);
  },
  getTopPay(device) {
    return httpService.getData(`${API.TOP_APPS}?device=${device}&type=pay`);
  }
}