import { combineReducers } from "redux";
import mainReducer from '../containers/Main/store/mainReducer';

export default combineReducers({
  mainReducer
});
