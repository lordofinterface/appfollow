import React from "react";
import ReactDOM from "react-dom";
import Main from "./containers/Main";
import { Provider } from "react-redux";
import createStore from './store/configureStore';
import 'semantic-ui-css/semantic.min.css';
import "./index.scss";

const store = createStore();

ReactDOM.render(
  <Provider store={store}>
    <Main />
  </Provider>,
  document.getElementById("root")
);
