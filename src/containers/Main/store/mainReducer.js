import * as actionTypes from "./mainActionTypes";

const initialState = {
  device: "iphone"
};

export default (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.CHANGE_DEVICE_TYPE:
      return {
        ...state,
        device: action.payload
      };

    case actionTypes.ADD_TOP_BANNERS:
      return {
        ...state,
        topBanners: action.payload
      };

    case actionTypes.ADD_APPS:
      return {
        ...state,
        apps: action.payload
      };

    case actionTypes.ADD_BANNER_COLLECTIONS:
      return {
        ...state,
        bannerCollections: action.payload
      };

    case actionTypes.ADD_APP_COLLECTIONS:
      return {
        ...state,
        appCollections: action.payload
      };

    case actionTypes.ADD_TOP_FREE:
      return {
        ...state,
        topFree: action.payload
      };

    case actionTypes.ADD_TOP_PAY:
      return {
        ...state,
        topPay: action.payload
      };

    default:
      return state;
  }
};
