import * as actionTypes from "./mainActionTypes";

export const changeDeviceType = device => ({
  type: actionTypes.CHANGE_DEVICE_TYPE,
  payload: device
});

export const addTopBanners = banners => ({
  type: actionTypes.ADD_TOP_BANNERS,
  payload: banners
});

export const addApps = apps => ({
  type: actionTypes.ADD_APPS,
  payload: apps
});

export const addBannerCollections = collections => ({
  type: actionTypes.ADD_BANNER_COLLECTIONS,
  payload: collections
});

export const addAppCollections = collections => ({
  type: actionTypes.ADD_APP_COLLECTIONS,
  payload: collections
});

export const addTopFree = apps => ({
  type: actionTypes.ADD_TOP_FREE,
  payload: apps
});

export const addTopPay = apps => ({
  type: actionTypes.ADD_TOP_PAY,
  payload: apps
});