import React, { Component } from "react";
import { connect } from "react-redux";
import { Grid } from "semantic-ui-react";
import * as apiHelper from "../../helpers/apiHelper";
import TopBanners from "../../components/TopBanners";
import Toggler from "../../components/common/Toggler";
import AppRow from "../../components/AppRow";
import BannersRow from "../../components/BannersRow";
import AppCollections from "../../components/AppCollections";
import TopApps from "../../components/TopApps";
import "./main.scss";

class Main extends Component {
  state = {};
  componentDidMount() {
    this.props.dispatch(apiHelper.getBannerCollections());
    this.props.dispatch(apiHelper.changeDeviceType("iphone"));
  }
  render() {
    const {
      topBanners,
      device,
      apps,
      bannerCollections,
      appCollections,
      topFree,
      topPay,
      dispatch
    } = this.props;
    return (
      <div className="main-container">
        <div className="main-container__row">
          <Toggler
            selectedRight={device === "ipad"}
            leftTitle={"IPhone"}
            rightTitle={"IPad"}
            leftClick={() => dispatch(apiHelper.changeDeviceType("iphone"))}
            rightClick={() => dispatch(apiHelper.changeDeviceType("ipad"))}
          />
        </div>
        <TopBanners topBanners={topBanners} />
        <div className="main-container__fix">
          <AppRow fullList apps={apps} />
          <Grid>
            <Grid.Column width={12}>
              <BannersRow banners={bannerCollections} />
              <AppCollections collections={appCollections} />
            </Grid.Column>
            <Grid.Column width={4}>
              <TopApps title={'Топ бесплатных'} apps={topFree} />
              <TopApps title={'Топ платных'} apps={topPay} />
            </Grid.Column>
          </Grid>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  device: state.mainReducer.device,
  topBanners: state.mainReducer.topBanners,
  apps: state.mainReducer.apps,
  appCollections: state.mainReducer.appCollections,
  bannerCollections: state.mainReducer.bannerCollections,
  topFree: state.mainReducer.topFree,
  topPay: state.mainReducer.topPay
});

const mapDispatchToProps = dispatch => ({
  dispatch: action => dispatch(action)
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Main);
