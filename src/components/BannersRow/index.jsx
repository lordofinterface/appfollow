import React, { Component } from "react";
import { Grid, Image } from "semantic-ui-react";
import "./banners-row.scss";

class BannersRow extends Component {
  render() {
    const { banners } = this.props;
    return (
      <div className="banners-row">
        <div className="banners-row__title">{banners && banners.title}</div>
        <Grid centered>
          {banners &&
            banners.items.map(banner => {
              return (
                <Grid.Column width={4} key={banner.id}>
                  <a className="banners-row__item" href={banner.link}>
                    <Image rounded src={banner.image} />
                  </a>
                </Grid.Column>
              );
            })}
        </Grid>
      </div>
    );
  }
}

export default BannersRow;
