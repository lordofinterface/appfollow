import React, { Component } from "react";
import { Grid, Image, Button } from "semantic-ui-react";
import "./app-row.scss";

class AppRow extends Component {
  render() {
    const { apps, count, fullInfo, fullList } = this.props;
    return (
      <div className="app-row">
        <Grid centered>
          {apps &&
            apps.map(app => {
              return ((!fullList && apps.indexOf(app) <= 7) || fullList)
                && (
                  <Grid.Column width={count || 2} key={app.id}>
                    <a className="app-row__item" href={app.link}>
                      <Image rounded src={app.icon} size="small" />
                    </a>
                    <div className="app-row__item__title">{app.name}</div>
                    <div className="app-row__item__category">
                      {app.category}
                    </div>
                    {fullInfo && (
                      <div>
                        <Button className="app-row__item__btn">
                          {app.price ? app.price + " р." : "Загрузить"}
                        </Button>
                        <div className="app-row__item__transactions">
                          {app.transactions
                            ? "Встроенные покупки"
                            : "Нет покупок"}
                        </div>
                      </div>
                    )}
                  </Grid.Column>
                );
            })}
        </Grid>
      </div>
    );
  }
}

export default AppRow;
