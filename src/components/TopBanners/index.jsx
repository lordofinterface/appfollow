import React, { Component } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import "./top-banners.scss";

class TopBanners extends Component {
  render() {
    const { topBanners } = this.props;
    const settings = {
      dots: true,
      arrow: true,
      infinite: true,
      speed: 500,
      autoplay: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: true
    };
    return (
      <div className="top-banners">
        <Slider {...settings}>
          {topBanners &&
            topBanners.map(banner => {
              return (
                <a href={banner.link} key={banner.id}>
                  <img src={banner.image} alt="" />
                </a>
              );
            })}
        </Slider>
      </div>
    );
  }
}

export default TopBanners;
