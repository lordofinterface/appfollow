import React, { Component } from "react";
import AppRow from "../AppRow";
import ModalApp from '../common/ModalApp';
import "./app-collections.scss";

class AppCollections extends Component {
  render() {
    const { collections } = this.props;
    return (
      <div className="app-collections">
        {collections &&
          collections.map(collection => {
            return (
              <div key={collection.id}>
                <div className="app-collections__title">
                  {collection.title}
                  <ModalApp title={collection.title} apps={collection.apps}/>
                </div>
                <AppRow fullInfo count={2} apps={collection.apps} />
              </div>
            );
          })}
      </div>
    );
  }
}

export default AppCollections;
