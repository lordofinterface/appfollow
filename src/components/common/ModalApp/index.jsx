import React, { Component } from "react";
import { Button, Modal } from "semantic-ui-react";
import AppRow from '../../AppRow';
import "./modal-app.scss";

class ModalApp extends Component {
  render() {
    const { apps, title } = this.props;
    return (
      <Modal trigger={<Button className='modal-button'>См. все</Button>}>
        <Modal.Header>{title}</Modal.Header>
        <Modal.Content>
          <AppRow fullList fullInfo apps={apps} />
        </Modal.Content>
      </Modal>
    );
  }
}

export default ModalApp;
