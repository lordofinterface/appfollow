import React from "react";
import { Button } from "semantic-ui-react";

const Toggler = (props) => (
  <Button.Group>
    <Button positive={!props.selectedRight} onClick={props.leftClick}>{props.leftTitle}</Button>
    <Button.Or />
    <Button onClick={props.rightClick} positive={props.selectedRight}>{props.rightTitle}</Button>
  </Button.Group>
);

export default Toggler;
