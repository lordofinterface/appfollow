import React, { Component } from "react";
import "./top-apps.scss";

class TopApps extends Component {
  render() {
    const { title, apps } = this.props;
    return (
      <div className="top-apps">
        <div className="top-apps__title">{title}</div>
        <ul>
          {apps &&
            apps.map((app, index) => {
              return (
                <li key={app.id}>
                  <a href={app.link} className="top-apps__item">
                    <div className="top-apps__item__number">{index + 1 + '.'}</div>
                    {apps.indexOf(app) === 0 && <img src={app.icon} alt="" />}
                    <div className="top-apps__info">
                      <div className="top-apps__info__title">{app.name}</div>
                      <div className="top-apps__info__category">
                        {app.category}
                      </div>
                    </div>
                  </a>
                </li>
              );
            })}
        </ul>
      </div>
    );
  }
}

export default TopApps;
