import * as mainActions from "../containers/Main/store/mainActions";
import appService from "../services/appService";

export function changeDeviceType(device) {
  return dispatch => {
    dispatch(mainActions.changeDeviceType(device));
    dispatch(getBanners(device));
    dispatch(getApps(device));
    dispatch(getAppCollections(device));
    dispatch(getTopFree(device));
    dispatch(getTopPay(device));
  };
}

export function getBanners(device) {
  return dispatch => {
    appService
      .getBanners(device)
      .then(res => dispatch(mainActions.addTopBanners(res)));
  };
}

export function getApps(device) {
  return dispatch => {
    appService.getApps(device).then(res => dispatch(mainActions.addApps(res)));
  };
}

export function getBannerCollections() {
  return dispatch => {
    appService
      .getBannerCollections()
      .then(res => dispatch(mainActions.addBannerCollections(res)));
  };
}

export function getAppCollections(device) {
  return dispatch => {
    appService
      .getAppCollections(device)
      .then(res => dispatch(mainActions.addAppCollections(res)));
  };
}

export function getTopFree(device) {
  return dispatch => {
    appService
      .getTopFree(device)
      .then(res => dispatch(mainActions.addTopFree(res)));
  };
}

export function getTopPay(device) {
  return dispatch => {
    appService
      .getTopPay(device)
      .then(res => dispatch(mainActions.addTopPay(res)));
  };
}
