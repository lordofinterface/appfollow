import shortid from "shortid";

export default {
  "top_banners?device=iphone": [
    {
      id: shortid.generate(),
      image:
        "http://a2.mzstatic.com/us/r30/Features122/v4/b8/6a/64/b86a646e-aca5-3fe0-225d-41f8ee1abb86/flowcase_1360_520_2x.jpeg",
      link: "#"
    },
    {
      id: shortid.generate(),
      image:
        "http://is2.mzstatic.com/image/thumb/Features111/v4/92/6a/b2/926ab2f9-0aab-f9b6-1225-bf7f4e4fb86d/source/1360x520fa.jpg",
      link: "#"
    },
    {
      id: shortid.generate(),
      image:
        "http://is1.mzstatic.com/image/thumb/Features111/v4/a5/da/02/a5da022a-b058-bf4e-dd3d-a9719dd14be4/source/1360x520fa.jpg",
      link: "#"
    }
  ],
  "apps?device=iphone": [
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
      name: "Bloons TD 6",
      category: "Games",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
      name: "Heads Up!",
      category: "Games",
      link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "Minecraft",
      category: "Games",
      link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Voodoo",
      category: "Games",
      link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
      name: "Telemundo Deportes - En Vivo",
      category: "Sports",
      link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
      name: "Instagram",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
      name: "Netflix",
      category: "Entertainment",
      link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
      name: "Pandora Music",
      category: "Music",
      link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
      name: "Facetune",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "HotSchedules",
      category: "Business",
      link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "HotSchedules",
      category: "Games",
      link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "YouTube: Watch, Listen, Stream",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Helix Jump!",
      category: "Action",
      link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
      name: "Poptropica",
      category: "Games",
      link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "Fortnite",
      category: "Games",
      link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "Tinder",
      category: "Lifestyle",
      link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
    }
  ],
  "app_collections?device=iphone": [
    {
      title: "Любителям головоломок",
      id: shortid.generate(),
      apps: [
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
          name: "Bloons TD 6",
          category: "Games",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
          name: "Heads Up!",
          category: "Games",
          link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "Minecraft",
          category: "Games",
          link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Voodoo",
          category: "Games",
          link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
          name: "Instagram",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
          name: "Netflix",
          category: "Entertainment",
          link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
          name: "Pandora Music",
          category: "Music",
          link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
          name: "Facetune",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "HotSchedules",
          category: "Business",
          link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "HotSchedules",
          category: "Games",
          link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "YouTube: Watch, Listen, Stream",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Helix Jump!",
          category: "Action",
          link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
          name: "Poptropica",
          category: "Games",
          link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
          name: "Telemundo Deportes - En Vivo",
          category: "Sports",
          link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Fortnite",
          category: "Games",
          link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Tinder",
          category: "Lifestyle",
          link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
        }
      ]
    },
    {
      title: "Все для работы с текстом",
      id: shortid.generate(),
      apps: [
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Helix Jump!",
          category: "Action",
          link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
          name: "Poptropica",
          category: "Games",
          link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
          name: "Telemundo Deportes - En Vivo",
          category: "Sports",
          link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
          name: "Bloons TD 6",
          category: "Games",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
          name: "Heads Up!",
          category: "Games",
          link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "Minecraft",
          category: "Games",
          link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Voodoo",
          category: "Games",
          link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
          name: "Instagram",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "HotSchedules",
          category: "Games",
          link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "YouTube: Watch, Listen, Stream",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Fortnite",
          category: "Games",
          link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Tinder",
          category: "Lifestyle",
          link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
          name: "Netflix",
          category: "Entertainment",
          link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
          name: "Pandora Music",
          category: "Music",
          link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
          name: "Facetune",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "HotSchedules",
          category: "Business",
          link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
        },
      ]
    },
    {
      title: "Наши новые фавориты",
      id: shortid.generate(),
      apps: [
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
          name: "Netflix",
          category: "Entertainment",
          link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
          name: "Pandora Music",
          category: "Music",
          link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
          name: "Facetune",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Helix Jump!",
          category: "Action",
          link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
          name: "Poptropica",
          category: "Games",
          link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "Minecraft",
          category: "Games",
          link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Voodoo",
          category: "Games",
          link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
          name: "Instagram",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "HotSchedules",
          category: "Games",
          link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "YouTube: Watch, Listen, Stream",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Fortnite",
          category: "Games",
          link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Tinder",
          category: "Lifestyle",
          link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "HotSchedules",
          category: "Business",
          link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
          name: "Telemundo Deportes - En Vivo",
          category: "Sports",
          link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
          name: "Bloons TD 6",
          category: "Games",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
          name: "Heads Up!",
          category: "Games",
          link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
        },
      ]
    }
  ],
  "top_banners?device=ipad": [
    {
      id: shortid.generate(),
      image:
        "http://is1.mzstatic.com/image/thumb/Features111/v4/a5/da/02/a5da022a-b058-bf4e-dd3d-a9719dd14be4/source/1360x520fa.jpg",
      link: "#"
    },
    {
      id: shortid.generate(),
      image:
        "http://a2.mzstatic.com/us/r30/Features122/v4/b8/6a/64/b86a646e-aca5-3fe0-225d-41f8ee1abb86/flowcase_1360_520_2x.jpeg",
      link: "#"
    },
    {
      id: shortid.generate(),
      image:
        "http://is2.mzstatic.com/image/thumb/Features111/v4/92/6a/b2/926ab2f9-0aab-f9b6-1225-bf7f4e4fb86d/source/1360x520fa.jpg",
      link: "#"
    }
  ],
  "apps?device=ipad": [
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
      name: "Netflix",
      category: "Entertainment",
      link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
      name: "Instagram",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "HotSchedules",
      category: "Games",
      link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
      name: "Pandora Music",
      category: "Music",
      link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
      name: "Facetune",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Helix Jump!",
      category: "Action",
      link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
      name: "Poptropica",
      category: "Games",
      link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "Minecraft",
      category: "Games",
      link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Voodoo",
      category: "Games",
      link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "YouTube: Watch, Listen, Stream",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "Fortnite",
      category: "Games",
      link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
      name: "Bloons TD 6",
      category: "Games",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
      name: "Heads Up!",
      category: "Games",
      link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "Tinder",
      category: "Lifestyle",
      link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "HotSchedules",
      category: "Business",
      link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
      name: "Telemundo Deportes - En Vivo",
      category: "Sports",
      link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
    },
  ],
  "app_collections?device=ipad": [
    {
      title: "Любителям головоломок",
      id: shortid.generate(),
      apps: [
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
          name: "Bloons TD 6",
          category: "Games",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
          name: "Heads Up!",
          category: "Games",
          link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "Minecraft",
          category: "Games",
          link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Voodoo",
          category: "Games",
          link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
          name: "Instagram",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
          name: "Netflix",
          category: "Entertainment",
          link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
          name: "Pandora Music",
          category: "Music",
          link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
          name: "Facetune",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "HotSchedules",
          category: "Business",
          link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "HotSchedules",
          category: "Games",
          link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "YouTube: Watch, Listen, Stream",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Helix Jump!",
          category: "Action",
          link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
          name: "Poptropica",
          category: "Games",
          link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
          name: "Telemundo Deportes - En Vivo",
          category: "Sports",
          link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Fortnite",
          category: "Games",
          link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Tinder",
          category: "Lifestyle",
          link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
        }
      ]
    },
    {
      title: "Все для разработчиков",
      id: shortid.generate(),
      apps: [
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Tinder",
          category: "Lifestyle",
          link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
          name: "Netflix",
          category: "Entertainment",
          link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
          name: "Pandora Music",
          category: "Music",
          link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
          name: "Facetune",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "HotSchedules",
          category: "Business",
          link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Helix Jump!",
          category: "Action",
          link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
          name: "Poptropica",
          category: "Games",
          link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
          name: "Telemundo Deportes - En Vivo",
          category: "Sports",
          link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
          name: "Bloons TD 6",
          category: "Games",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
          name: "Heads Up!",
          category: "Games",
          link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "Minecraft",
          category: "Games",
          link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Voodoo",
          category: "Games",
          link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
          name: "Instagram",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "HotSchedules",
          category: "Games",
          link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "YouTube: Watch, Listen, Stream",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Fortnite",
          category: "Games",
          link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
          price: 60,
          transactions: true
        },
      ]
    },
    {
      title: "Все для работы с текстом",
      id: shortid.generate(),
      apps: [
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
          name: "Netflix",
          category: "Entertainment",
          link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
        },
        
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
          name: "Poptropica",
          category: "Games",
          link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "Minecraft",
          category: "Games",
          link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Voodoo",
          category: "Games",
          link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
          name: "Instagram",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
        },{
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
          name: "Pandora Music",
          category: "Music",
          link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
          name: "Facetune",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
          name: "Helix Jump!",
          category: "Action",
          link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
          name: "HotSchedules",
          category: "Games",
          link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "YouTube: Watch, Listen, Stream",
          category: "Photo & Video",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Fortnite",
          category: "Games",
          link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "Tinder",
          category: "Lifestyle",
          link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
          name: "HotSchedules",
          category: "Business",
          link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
          name: "Telemundo Deportes - En Vivo",
          category: "Sports",
          link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
        },
        {
          id: shortid.generate(),
          icon:
            "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
          name: "Bloons TD 6",
          category: "Games",
          link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
          price: 60,
          transactions: true
        },
        {
          id: shortid.generate(),
          icon:
            "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
          name: "Heads Up!",
          category: "Games",
          link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
        },
      ]
    }
  ],
  "top_apps?device=iphone&type=pay": [
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
      name: "Facetune",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Helix Jump!",
      category: "Action",
      link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "HotSchedules",
      category: "Games",
      link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "YouTube: Watch, Listen, Stream",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "Fortnite",
      category: "Games",
      link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "Tinder",
      category: "Lifestyle",
      link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "HotSchedules",
      category: "Business",
      link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
      name: "Telemundo Deportes - En Vivo",
      category: "Sports",
      link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
      name: "Bloons TD 6",
      category: "Games",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
      name: "Heads Up!",
      category: "Games",
      link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
    },
  ],
  "top_apps?device=iphone&type=free": [
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
      name: "Netflix",
      category: "Entertainment",
      link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
    },
    
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
      name: "Poptropica",
      category: "Games",
      link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "Minecraft",
      category: "Games",
      link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Voodoo",
      category: "Games",
      link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
      name: "Bloons TD 6",
      category: "Games",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
      name: "Instagram",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
    },{
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
      name: "Pandora Music",
      category: "Music",
      link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
      name: "Facetune",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Helix Jump!",
      category: "Action",
      link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "HotSchedules",
      category: "Games",
      link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
      price: 60,
      transactions: true
    },
  ],

  "top_apps?device=ipad&type=pay": [
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/14/db/6a/14db6a5f-1dc9-ee84-866d-bcd3edd3d25f/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-9.png/230x0w.jpg",
      name: "Netflix",
      category: "Entertainment",
      link: "https://itunes.apple.com/us/app/netflix/id363590051?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
      name: "Bloons TD 6",
      category: "Games",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
      price: 60,
      transactions: true
    },
    
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/8c/7f/a1/8c7fa1f4-8d16-eaaa-45a9-794e82f1e0c4/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-8.png/230x0w.jpg",
      name: "Poptropica",
      category: "Games",
      link: "https://itunes.apple.com/us/app/poptropica/id818709874?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "Minecraft",
      category: "Games",
      link: "https://itunes.apple.com/us/app/minecraft/id479516143?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/e8/0c/8b/e80c8bf5-1e8e-0fe8-fc42-f33712ef0222/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Voodoo",
      category: "Games",
      link: "https://itunes.apple.com/us/app/hole-io/id1389111413?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple125/v4/b0/f8/f7/b0f8f7b9-ec5d-8317-5be9-98090685b0bc/Prod-1x_U007emarketing-85-220-0-5.png/690x0w.jpg",
      name: "Instagram",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/instagram/id389801252?mt=8"
    },{
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/4c/a7/a9/4ca7a941-cf16-bfa8-1c58-c2cfdbb6d600/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/460x0w.jpg",
      name: "Pandora Music",
      category: "Music",
      link: "https://itunes.apple.com/us/app/pandora-music/id284035177?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
      name: "Facetune",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Helix Jump!",
      category: "Action",
      link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "HotSchedules",
      category: "Games",
      link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
      price: 60,
      transactions: true
    },
  ],

  "top_apps?device=ipad&type=free": [
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple125/v4/3e/70/ed/3e70edf5-d409-7fcb-2470-dfb2cfe304a0/AppIcon-1x_U007emarketing-85-220-2.png/230x0w.jpg",
      name: "Facetune",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/facetune/id606310581?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is2-ssl.mzstatic.com/image/thumb/Purple115/v4/95/eb/27/95eb274f-80b1-0adb-a9f4-c0b81a628bfb/AppIcon-1x_U007emarketing-0-85-220-0-8.png/230x0w.jpg",
      name: "Helix Jump!",
      category: "Action",
      link: "https://itunes.apple.com/us/app/helix-jump/id1345968745?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2e/34/9e/2e349ead-d1f8-8998-edb0-4f8ddd9ce451/mzl.ytvqcsfj.png/690x0w.jpg",
      name: "HotSchedules",
      category: "Games",
      link: "https://itunes.apple.com/us/app/youtube-watch-listen-stream/id544007664?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/dc/f3/56/dcf35671-0267-29ea-5b5d-681f7c6648cb/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "YouTube: Watch, Listen, Stream",
      category: "Photo & Video",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple125/v4/03/eb/83/03eb83e7-8885-8124-3488-616c64503195/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "Fortnite",
      category: "Games",
      link: "https://itunes.apple.com/us/app/fortnite/id1261357853?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/aa/ca/9d/aaca9d4c-4db8-8630-8bd4-1dacdaf88fa1/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "Tinder",
      category: "Lifestyle",
      link: "https://itunes.apple.com/us/app/tinder/id547702041?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is4-ssl.mzstatic.com/image/thumb/Purple115/v4/38/9f/4e/389f4e11-bef2-69f3-48a5-e7598ef808ce/AppIcon-1x_U007emarketing-85-220-0-6.png/230x0w.jpg",
      name: "HotSchedules",
      category: "Business",
      link: "https://itunes.apple.com/us/app/hotschedules/id294934058?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple125/v4/74/67/16/746716a8-ef1c-bb7a-dc47-c512d973ea68/wc-appIcon-1x_U007emarketing-0-P3-85-220-0-8.png/460x0w.jpg",
      name: "Telemundo Deportes - En Vivo",
      category: "Sports",
      link: "https://itunes.apple.com/us/app/telemundo-deportes-en-vivo/id686218010?mt=8"
    },
    {
      id: shortid.generate(),
      icon:
        "https://is3-ssl.mzstatic.com/image/thumb/Purple115/v4/2f/55/8b/2f558b65-ef82-e086-800f-332b7e9b9c71/AppIcon-1x_U007emarketing-0-0-GLES2_U002c0-512MB-sRGB-0-0-0-85-220-0-0-0-6.png/230x0w.jpg",
      name: "Bloons TD 6",
      category: "Games",
      link: "https://itunes.apple.com/us/app/bloons-td-6/id1118115766?mt=8",
      price: 60,
      transactions: true
    },
    {
      id: shortid.generate(),
      icon:
        "https://is1-ssl.mzstatic.com/image/thumb/Purple115/v4/72/e3/72/72e37205-d35e-5e47-669b-7215efafedff/AppIcon-1x_U007emarketing-85-220-6.png/460x0w.jpg",
      name: "Heads Up!",
      category: "Games",
      link: "https://itunes.apple.com/us/app/heads-up/id623592465?mt=8"
    },
  ],

  banner_collections: {
    title: "Популярные коллекции",
    items: [
      {
        id: shortid.generate(),
        image:
          "http://is2.mzstatic.com/image/thumb/Features111/v4/26/60/eb/2660eb6a-366a-28d6-29f6-632b76cdaa49/source/400x196fa.jpg",
        link: "#"
      },
      {
        id: shortid.generate(),
        image:
          "http://is4.mzstatic.com/image/thumb/Features127/v4/de/8c/9e/de8c9eb1-cb1e-2c50-00e0-063deafe6bf6/source/400x196fa.jpg",
        link: "#"
      },
      {
        id: shortid.generate(),
        image:
          "http://is1.mzstatic.com/image/thumb/Features122/v4/49/fa/3f/49fa3f55-fa58-f57c-01db-cf356d068cfb/source/400x196fa.jpg",
        link: "#"
      },
      {
        id: shortid.generate(),
        image:
          "http://is2.mzstatic.com/image/thumb/Features111/v4/26/60/eb/2660eb6a-366a-28d6-29f6-632b76cdaa49/source/400x196fa.jpg",
        link: "#"
      },
      {
        id: shortid.generate(),
        image:
          "http://is4.mzstatic.com/image/thumb/Features127/v4/de/8c/9e/de8c9eb1-cb1e-2c50-00e0-063deafe6bf6/source/400x196fa.jpg",
        link: "#"
      },
      {
        id: shortid.generate(),
        image:
          "http://is1.mzstatic.com/image/thumb/Features122/v4/49/fa/3f/49fa3f55-fa58-f57c-01db-cf356d068cfb/source/400x196fa.jpg",
        link: "#"
      },
      {
        id: shortid.generate(),
        image:
          "http://is1.mzstatic.com/image/thumb/Features122/v4/49/fa/3f/49fa3f55-fa58-f57c-01db-cf356d068cfb/source/400x196fa.jpg",
        link: "#"
      },
      {
        id: shortid.generate(),
        image:
          "http://is2.mzstatic.com/image/thumb/Features111/v4/26/60/eb/2660eb6a-366a-28d6-29f6-632b76cdaa49/source/400x196fa.jpg",
        link: "#"
      }
    ]
  }
};
